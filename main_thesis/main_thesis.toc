\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Motivation}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Feedforward Neural Network}{3}{chapter.3}
\contentsline {section}{\numberline {3.1}Build phase}{3}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Layer}{4}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Activation functions}{4}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Cost function}{7}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Training phase}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Gradient descent algorithm}{8}{subsection.3.2.1}
\contentsline {chapter}{\numberline {4}The \texttt {quicknn} package}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Prerequisites}{10}{section.4.1}
\contentsline {section}{\numberline {4.2}How to install the package}{10}{section.4.2}
\contentsline {section}{\numberline {4.3}How to use the package}{11}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}First example: Description of the parameters}{11}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Second example: How to stop-resume the training}{15}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Visualizing learning curves using Tensorboard}{21}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Conclusions and future works}{24}{chapter.5}
\contentsline {chapter}{Bibliography}{25}{chapter.5}
